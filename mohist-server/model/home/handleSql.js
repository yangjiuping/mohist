const mysql = require('mysql')
const config = require('../../config')

//该数组用户存放sql连接池
const sqlList = []

function SqlConfig() {
    // 判断当前连接池是否有可用的，如果没有就新建一个
    if (!sqlList[0] || !sqlList.pop().isUse) {
        sqlList.push({
            connect: mysql.createConnection( config.sqlConfig ),
            isUse: true
        })
    }
    return sqlList.pop()
}

exports.ReadChinaMap = function (callback) {
    //读取 chinaMap 的数据
    let con = SqlConfig()
    var connection = con.connect
    connection.query('SELECT * FROM home_chinamap', function (err, results) {
        if (err) return callback(err, null);
        con.isUse = false
        sqlList.splice(sqlList.indexOf(con), 1)
        sqlList.push(con)
        connection.end();
        return callback(null, results);
    });
}


let newData = []
exports.InsetChinaMap = function (callback) {
    //导入 chinaMap 的数据
    let con = SqlConfig()
    var connection = con.connect
    //增添数据
    (function iterator(i) {
        console.log(i)
        if (i == newData.length) {
            console.log('结束')
            // connection.end();
            return callback(null, []);
        }


        var addSql = 'INSERT INTO home_chinamap(name, value, latitude, longitude) VALUES(?, ?, ?, ?)'
        var addSqlParams = [newData[i].name, newData[i].value, newData[i].latitude, newData[i].longitude]
        connection.query(addSql, addSqlParams, function (err, results) {
            if (err) return callback(err, null);
            i++
            iterator(i)
        });
    })(0);

}

exports.SubmitForm = function(param, callback){
    let con = SqlConfig()
    var connection = con.connect
    var addSql = 'INSERT INTO home_submitform(name, email, idea) VALUES(?, ?, ?)'
    var addSqlParams = [param.name, param.email, param.idea]
    connection.query(addSql, addSqlParams, function (err, results) {
        if (err) return callback(err, null);

        con.isUse = false
        sqlList.splice(sqlList.indexOf(con), 1)
        sqlList.push(con)
        connection.end();
        return callback(null, results);
    });
}

