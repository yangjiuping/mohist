const mysql = require('mysql')
const config = require('../../config')

//该数组用户存放sql连接池
const sqlList = []

function SqlConfig() {
    // 判断当前连接池是否有可用的，如果没有就新建一个
    if (!sqlList[0] || !sqlList.pop().isUse) {
        sqlList.push({
            connect: mysql.createConnection( config.sqlConfig ),
            isUse: true
        })
    }
    return sqlList.pop()
}

exports.ReadPromission = function (callback) {
    //读取 chinaMap 的数据
    let con = SqlConfig()
    var connection = con.connect
    connection.query('SELECT * FROM promission', function (err, results) {
        if (err) return callback(err, null);
        con.isUse = false
        sqlList.splice(sqlList.indexOf(con), 1)
        sqlList.push(con)
        connection.end();
        return callback(null, results);
    });
}


