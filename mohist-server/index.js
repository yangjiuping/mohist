const express = require('express')
const app = express()
const config = require('./config')
const bodyParser = require('body-parser')
const Home = require('./controller/home')
const Promission = require('./controller/promission')


//bodyParser API
// 解析 application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// 解析 application/json
app.use(bodyParser.json())

//home页
//查询地图
app.get('/home/chinaMap', Home.ChinaMap);
//插入地图数据
app.get('/home/insertChinaMap', Home.InsetChinaMap);
//意见表单提交
app.post('/home/form', Home.SubmitForm);


//查詢 promission
app.get('/promission/list', Promission.List);

app.use(function(req, res){
    // res.write('404');
    var resJson = config.resJson('404', '无结果', 'success')
    res.json(resJson)
});
app.listen('8888');