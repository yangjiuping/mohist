const HandleSql = require('../model/home/handleSql')
const Url = require('url')
const QueryString = require('querystring')
const config = require('../config')
exports.ChinaMap = function (req, res, next) {
    HandleSql.ReadChinaMap((err, result) => {
        if (err) {
            // next();
            var resJson = config.resJson([], 'fail', 'fail')
            res.json(resJson)
            return;
        }
        var resJson = config.resJson(result, '查询成功', 'success')
        res.json(resJson)
    });
}

exports.InsetChinaMap = function (req, res, next) {
    HandleSql.InsetChinaMap((err, result) => {
        if (err) {
            var resJson = config.resJson([], 'fail', 'fail')
            res.json(resJson)
            return;
        }
        var resJson = config.resJson(result, '添加成功', 'success')
        res.json(resJson)
    });
}
//表单提交
exports.SubmitForm = function (req, res, next) {
    let params = req.body;
    if(params.name && params.email && params.idea){
        HandleSql.SubmitForm(params, (err, result) => {
            if (err) {
                var resJson = config.resJson("fail", '提交失败', 'fail')
                res.json(resJson)
                return;
            }
            var resJson = config.resJson("success", '提交成功，感谢你的宝贵意见', 'success')
            res.json(resJson)
        })
    }else{
        var resJson = config.resJson("fail", '必填项不能为空', 'fail')
        res.json(resJson)
    }
}
