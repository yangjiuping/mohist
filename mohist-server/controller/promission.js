const HandleSql = require('../model/promission/handleSql')
const Url = require('url')
const QueryString = require('querystring')
const config = require('../config')

exports.List = function (req, res, next) {
    HandleSql.ReadPromission((err, result) => {
        if (err) {
            var resJson = config.resJson([], 'fail', 'fail')
            return res.json(resJson)
        }
        var resJson = config.resJson(result, '查询成功', 'success')
        return res.json(resJson)
    });
}

