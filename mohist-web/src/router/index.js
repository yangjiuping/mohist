import Vue from 'vue'
import Router from 'vue-router'

import learn from '@/router/learn' //学无止境
import project from '@/router/project' //项目
import resume from '@/router/resume' //个人简历
import exercise from '@/router/exercise' //练习


Vue.use(Router)

let constantRouter = [{
        path: '/',
        redirect: '/index'
    },
    {
        path: '/index',
        component: () =>
            import ('@/vivews/index'),
    },
    {
        path: '/404',
        component: () =>
            import ('@/vivews/err')
    },
    {
        path: '*',
        redirect: '/404'
    }
]

export default new Router({
    mode: 'hash',
    routes: [
        ...constantRouter,
        // ...learn,
        // ...project,
        // ...resume,
        // ...exercise
    ]
})