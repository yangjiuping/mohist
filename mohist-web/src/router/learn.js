export default [{
    name: 'learn',
    path: '/learn',
    component: () => import('@/vivews/learn'),
    dropMenu: [
        {
            name: 'css',
            title: 'css'
        },
        {
            name: 'vue',
            title: 'vue'
        },
        {
            name: 'element',
            title: 'element'
        }
    ],
    children: []


}]