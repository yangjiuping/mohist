export default [{
    name: 'exercise',
    path: '/exercise',
    component: () => import('@/vivews/exercise'),
    dropMenu: [
        {
            name: 'css',
            title: 'css'
        },
        {
            name: 'vue',
            title: 'vue'
        },
        {
            name: 'element',
            title: 'element'
        }
    ],
    children: [
        {
            path: '/exercise/learnSlot',
            name: 'slot学习',
            component: () => import('@/vivews/exercise/learnSlot')
        },
        {
            path: '/exercise/rulesForm',
            name: 'rulesForm',
            component: () => import('@/vivews/exercise/rulesForm')
        },
        {
            path: '/exercise/assignment',
            name: 'assignment',
            component: () => import('@/vivews/exercise/assignment')
        },
        {
            path: '/exercise/transfer',
            name: 'transfer',
            component: () => import('@/vivews/exercise/transfer')
        },
        {
            path: '/exercise/treeTable',
            name: 'treeTable',
            component: () => import('@/vivews/exercise/treeTable')
        },
        {
            path: '/exercise/clickOutside',
            name: 'clickOutside',
            component: () => import('@/vivews/exercise/clickOutside')
        },
        {
            path: '/exercise/canvas',
            name: 'canvas',
            component: () => import('@/vivews/exercise/canvas')
        },
        {
            path: '/exercise/safetyGreenCross',
            name: '安全绿十字',
            component: () => import('@/vivews/exercise/safetyGreenCross')
        },
        {
            path: '/exercise/jszip',
            name: 'jszip',
            component: () => import('@/vivews/exercise/jszip')
        },
        {
            path: '/exercise/modelBind',
            name: '双向绑定',
            component: () => import('@/vivews/exercise/modelBind')
        },
        {
            path: '/exercise/uploadFile',
            name: '上传文件',
            component: () => import('@/vivews/exercise/uploadFile')
        },
        {
            path: '/exercise/qrcode',
            name: '二维码扫描',
            component: () => import('@/vivews/exercise/qrcode')
        },
        {
            path: '/exercise/videoPic',
            name: '展示视频流',
            component: () => import('@/vivews/exercise/videoPic')
        },

    ]


}]