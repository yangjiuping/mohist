import axios from 'axios'
import { Message } from 'element-ui'
import store from '../store'

// 创建axios实例
const service = axios.create({
	baseURL: process.env.BASE_API,
    timeout: 5000, // 请求超时时间
    headers: { 'Content-Type': 'application/json; charset=utf-8' }
})

// 添加请求拦截器 request拦截器 再发送请求之前做些什么
service.interceptors.request.use(
    config => {
        console.log(config)
        if (store.getters.token) {
            config.headers['Authorization'] = 'aaa' // 让每个请求携带自定义token 请根据实际情况自行修改
        }
        return config
    },
    error => {
        // Do something with request error
        console.log(error) // for debug
        Promise.reject(error)
    }
)

// 添加响应拦截器 response 拦截器 在拿到数据之后做些什么
service.interceptors.response.use(
    response => {
        /**
         * code为非20000是抛错 可结合自己业务进行修改
         */
        const res = response.data
        return res
    },
    error => {
        Message({
            message: error.response.data.message,
            type: 'error',
            duration: 5 * 1000
        })
        return Promise.reject(error)
    }
)

export default service