import { get, post } from './config.js';


var api = "/api";   //


export default {
    LoginOut(params){
        return get(api +"?format=json3", params)
    },

    GetChinaMap(params){
        //获取中国地图数据
        return get(api + "/home/chinaMap", params)
    },
    HomeSubmitForm(params){
        //首页提交意见表单
        console.log(params)
        return post(api + "/home/form", params)
    }
}

