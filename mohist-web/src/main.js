// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

//解决ie无法显示问题
import "babel-polyfill"

//引入element ui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI);

//使用动画
import animated from 'animate.css'
Vue.use(animated)

//路由配置
import router from './router'

//反向代理配置
import api from './axios/api.js'
Vue.prototype.$axios = api

//配置vuex
import store from './store/index'

/*引入公共样式*/
import '@/style/public.css'
/*引入公共图标*/
import '@/style/icon.css';



Vue.config.productionTip = false

//获取权限，动态路由配置
import './promission'

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    // components: {App},
    // template: '<App/>'
})