import Vue from 'vue'
import Vuex from 'vuex'


import Public from '@/store/modules_public/index'

Vue.use(Vuex)


export default new Vuex.Store({
    modules: {
        Public
    }
})
