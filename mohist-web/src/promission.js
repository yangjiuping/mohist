import router from './router'
import axios from 'axios'
import exercise from '@/router/exercise' //练习
import loginApi from "@/api/login"
import { LocalStorage } from "@/util/mohist"

let getRouter = null; //用来获取后台拿到的路由

router.beforeEach((to, from, next) => {
    if (!getRouter) {
        if (LocalStorage.GetLocalStorage('router')) {
            //不加这个判断，路由会陷入死循环 从localStorage拿到了路由
            getRouter = LocalStorage.GetLocalStorage('router');//拿到路由
            promissionRouter.routerGo(to, next);
        }
        else {
            loginApi.promission().then(res => {
                getRouter = res.row;//后台拿到路由
                LocalStorage.SetLocalStorage('router', getRouter);//存到本地，防止用戶查新
                promissionRouter.routerGo(to, next);//执行路由跳转方法
            }).catch(err => {
                console.log(err);
            });
        }
    } else {
        next();
    }
})

let promissionRouter = {
    routerGo(to, next) {
        getRouter = this.filterAsyncRouter(getRouter); //过滤路由
        router.addRoutes(getRouter); //动态添加路由 
        next({ ...to, replace: true });
    },
    filterAsyncRouter(asyncRouterMap) { //遍历后台传来的路由字符串，转换为组件对象
        const accessedRouters = asyncRouterMap.filter(route => {
            if (route.path) {
                //根据路径定位到组件
                route.component = () => import(`@/vivews${route.path}`);
            }
            if (route.children && route.children.length) {
                route.children = promissionRouter.filterAsyncRouter(route.children);
            }
            return true
        })
        return accessedRouters;
    }
}